package cn.zg.query;

import lombok.Data;

@Data
public class DepartQuery {
    private Integer currentPage;
    private Integer pageSize;
    private String name ;
}
