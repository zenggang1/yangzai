package cn.zg.util;

import lombok.Data;

import java.util.List;

@Data
public class PageList<T> {
    private Long total;
    private List<T> rows;

    public PageList(Long total, List<T> rows) {
        this.total = total;
        this.rows = rows;
    }

    public PageList() {
    }
}
