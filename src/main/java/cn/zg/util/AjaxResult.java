package cn.zg.util;

import ch.qos.logback.core.net.SyslogOutputStream;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data

public class AjaxResult {
    private Boolean success = true;
    private String msg;
    private Object data;

    public static AjaxResult success(String msg) {
        AjaxResult result = new AjaxResult();
        result.setSuccess(true);
        result.setMsg(msg);
        return result;
    }




}


