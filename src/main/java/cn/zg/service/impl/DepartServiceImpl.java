package cn.zg.service.impl;

import cn.zg.domain.Department;
import cn.zg.mapper.DepartmentMapper;
import cn.zg.query.DepartQuery;
import cn.zg.service.IDepartService;
import cn.zg.util.AjaxResult;
import cn.zg.util.PageList;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
public class DepartServiceImpl implements IDepartService {
    @Autowired
    private DepartmentMapper departmentMapper;

    @Override
    public List<Department> findAll() {
        return departmentMapper.findAll();
    }

    @Override
    @Transactional
    public  void save(Department department) {
        departmentMapper.save(department);
    }

    @Override
    public AjaxResult pageList(DepartQuery departQuery) {
        //开启分页
        PageHelper.startPage(departQuery.getCurrentPage(), departQuery.getPageSize());

        PageInfo<Department> pageInfo = new PageInfo<>(departmentMapper.findByQuery(departQuery));
        PageList<Department> pageList = new PageList(pageInfo.getTotal(), pageInfo.getList());

        AjaxResult res = AjaxResult.success("操作成功");
        res.setData(pageList);
        return res;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        departmentMapper.delete(id);
    }


}
