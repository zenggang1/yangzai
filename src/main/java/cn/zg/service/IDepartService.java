package cn.zg.service;

import cn.zg.domain.Department;
import cn.zg.query.DepartQuery;
import cn.zg.util.AjaxResult;

import java.util.List;

public interface IDepartService {
    List<Department> findAll();

    void save(Department department);

    AjaxResult pageList(DepartQuery departQuery);


    void delete(Long id);
}
