package cn.zg.controller;


import cn.zg.domain.DeleteId;
import cn.zg.domain.Department;
import cn.zg.query.DepartQuery;
import cn.zg.service.IDepartService;
import cn.zg.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dept")
public class DepartController {

    @Autowired
    private IDepartService departService;

    //查询所有数据
//    @GetMapping
//    public List<Department> findAll() {
//        return departService.findAll();
//    }

    //删除的方法
    @PostMapping("/del")
    public AjaxResult delete(@RequestBody DeleteId deleteId){
            departService.delete(deleteId.getId());
            return new AjaxResult();

    }

    //增加
    @PostMapping("/save")
    public AjaxResult save(@RequestBody Department department){
         departService.save(department);
         return AjaxResult.success("操作成功");

    }

    //分页
    //    @RequestBody:前端传以一个的对象到后台，必须要此注解
    @PostMapping("/query")
    public AjaxResult pageList(@RequestBody DepartQuery departQuery){
        System.out.println(1);
        System.out.println(departQuery);
        return departService.pageList(departQuery);

    }

}
