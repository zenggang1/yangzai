package cn.zg;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//启动类
@SpringBootApplication
@MapperScan("cn.zg.mapper")
public class petApp {
    public static void main(String[] args) {
        SpringApplication.run(petApp.class);
    }
}
