package cn.zg.domain;


import lombok.Data;

@Data
public class DeleteId {
    private Long id;

}
