package cn.zg.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class Department {

    @ApiModelProperty(value = "自增长主键")
    private Long id;
    @ApiModelProperty(value = "sn编号")
    private String sn;
    @ApiModelProperty(value = "部门名称")
    private String name;
    private Integer state;
    private Long managerId;
    private Long parentId;


}
