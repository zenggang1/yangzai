package cn.zg.mapper;

import cn.zg.domain.Department;
import cn.zg.query.DepartQuery;
import cn.zg.util.AjaxResult;

import java.util.List;

public interface DepartmentMapper {

    List<Department> findAll();

    void save(Department department);

    List<Department> findByQuery(DepartQuery departQuerye);


    void delete(Long id);
}
